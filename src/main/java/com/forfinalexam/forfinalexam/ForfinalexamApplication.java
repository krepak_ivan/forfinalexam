package com.forfinalexam.forfinalexam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ForfinalexamApplication {

	public static void main(String[] args) {
		SpringApplication.run(ForfinalexamApplication.class, args);
	}

}
